# Adrianwebdev

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.2.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Create a module by `ng g m <feature-name>/<ModuleName> --flat --routing --project <project-name>`:

```
ng g m +dashboard/Dashboard --flat --routing --project adrianwebdev
```

Create a component by `ng g c <feature-name>/<ComponentName> --flat --project <project-name> -c onPush -m <module-name>`:

```
ng g c +dashboard/DashboardPage --flat --project adrianwebdev -c onPush -m dashboard.module
```

Create a service by `ng g s <feature-name>/<ServiceName> --flat --project <project-name> -m <module-name>`:

```
ng g s +dashboard/Dashboard --flat --project adrianwebdev -m dashboard.module
```

Create a directive by `ng g d <feature-name>/<DirectiveName> --flat --project <project-name> -m <module-name>`:

```
ng g d +dashboard/Dimensions --flat --project adrianwebdev -m dashboard.module
```

Create a pipe by `ng g p <feature-name>/<PipeName> --flat --project <project-name> -m <module-name>`:

```
ng g p +dashboard/UTCDate --flat --project adrianwebdev -m dashboard.module
```

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
