import { Component } from '@angular/core';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent {

  constructor() { }
  isView: boolean = false;
  elementClass: string = "";

  viewImage(classList: string): void {
    this.elementClass = classList;
    this.isView = this.isView == false ? true : false;
  }

}
