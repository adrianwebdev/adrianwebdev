import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationModule } from './+commons/+navigation/navigation.module';
import { AboutModule } from './+pages/+about/about.module';
import { HomeModule } from './+pages/+home/home.module';
import { ContactModule } from './+pages/+contact/contact.module';
import { ProjectsModule } from './+pages/+projects/projects.module';
import { SkillsModule } from './+pages/+skills/skills.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AboutModule,
    HomeModule,
    ContactModule,
    ProjectsModule,
    SkillsModule,
    NavigationModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
