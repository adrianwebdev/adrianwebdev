import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home', 
    loadChildren: () => import('./+pages/+home/home.module').then(m => m.HomeModule)
  },
  {
    path: 'about-me',
    loadChildren: () => import('./+pages/+about/about.module').then(m => m.AboutModule)
  },
  {
    path: 'skills',
    loadChildren: () => import('./+pages/+skills/skills.module').then(m => m.SkillsModule)
  },
  {
    path: 'projects',
    loadChildren: () => import('./+pages/+projects/projects.module').then(m => m.ProjectsModule)
  },
  {
    path: 'contact-me',
    loadChildren: () => import('./+pages/+contact/contact.module').then(m => m.ContactModule)
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: 'home'
  }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(
      routes,
      {
          preloadingStrategy: PreloadAllModules
      }
    )
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
