import { Component, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  
  turnSticky: boolean = false;
  isOpen: boolean = false;

  constructor() { }
  
  ngOnInit(): void {
  }

  @HostListener("window:scroll", ["$event"])
  onWindowScroll() {
    let pos = (document.documentElement.scrollTop || document.body.scrollTop) + document.documentElement.offsetHeight;
    let max = window.innerHeight * 2 - 52;
    this.turnSticky = pos >= max;
  }

  goTo(page: string) {
    document.getElementById(page)?.scrollIntoView({behavior: "smooth"});
  }

  openMenu() {
    this.isOpen = this.isOpen ? false : true;
  }
}
